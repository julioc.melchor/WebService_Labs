﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class OptionRepository : IOptionRepository
    {
        public int Add(Option option)
        {
            using (var context = new SurveyDbContext())
            {
                context.Options.Add(option);
                context.SaveChanges();
                option.CreatedDate = DateTime.Now;
                option.ModifiedDate = null;
                return option.Id;
            }
        }
        public bool Delete(int id)
        {
            using (var ctx = new SurveyDbContext())
            {
                Option option = ctx.Options.AsNoTracking().SingleOrDefault(x => x.Id == id);
                if (option == null) return false;

                ctx.Entry(option).State = EntityState.Modified;

                ctx.SaveChanges();

                return true;
            }
        }

        public Option Get(int id)
        {
            using (var ctx = new SurveyDbContext())
            {
                return ctx.Options.AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }

        public bool Update(Option option)
        {
            using (var ctx = new SurveyDbContext())
            {
                Option currentOption = ctx.Options.SingleOrDefault(x => x.Id == option.Id);

                if (currentOption == null) return false;

                currentOption.Text = option.Text;
                currentOption.ModifiedDate = DateTime.Now;

                ctx.Entry(currentOption).State = EntityState.Modified;

                ctx.SaveChanges();

                return true;
            }
        }

    }
}
