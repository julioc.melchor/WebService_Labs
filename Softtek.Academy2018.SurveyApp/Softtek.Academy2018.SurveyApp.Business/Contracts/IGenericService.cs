using Softtek.Academy2018.SurveyApp.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Softtek.Academy2018.SurveyApp.Business.Contracts
{
public interface IGenericService<T>
{
int Add(T entity);
T Get(int id);
bool Update(T entity);
bool Delete(int id);
}
}