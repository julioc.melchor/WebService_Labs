﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Data.Entity;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class SurveyRepository : ISurveyRepository
    {
        public int Add(Survey survey)
        {
            using (var ctx = new SurveyDbContext())
            {
                survey.CreatedDate = DateTime.Now;
                survey.ModifiedDate = null;
                //user.ProjectId = 1;
                ctx.Surveys.Add(survey);       
                ctx.SaveChanges();
                return survey.Id;
            }
        }

        public bool Delete(int id)
        {
            using (var ctx = new SurveyDbContext())
            {
                Survey survey = ctx.Surveys.AsNoTracking().SingleOrDefault(x => x.Id == id);
                if (survey == null) return false;

                survey.IsArchived = true;

                ctx.Entry(survey).State = EntityState.Modified;

                ctx.SaveChanges();

                return true;
            }
        }

        public Survey Get(int id)
        {
            using (var ctx = new SurveyDbContext())
            {
                return ctx.Surveys.AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }

        public bool Update(Survey survey)
        {
            
            using (var ctx = new SurveyDbContext())
            {
                Survey currentSurvey = ctx.Surveys.SingleOrDefault(x=>x.Id==survey.Id);

                if (currentSurvey == null) return false;

                currentSurvey.Title = survey.Title;
                currentSurvey.Description = survey.Description;
                currentSurvey.ModifiedDate = DateTime.Now;

                ctx.Entry(currentSurvey).State = EntityState.Modified;

                ctx.SaveChanges();

                return true;
            }
        }
        }
    }
}
