﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Data.Entity;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class QuestionRepository : IQuestionRepository
    {
        public int Add(Question survey)
        {
            using (var context = new SurveyDbContext())
            {
                context.Questions.Add(survey);
                context.SaveChanges();
                return survey.Id;
            }
        }

        public bool Delete(int id)
        {
            using (var ctx = new SurveyDbContext())
            {
                Option currentQuestion = ctx.Options.AsNoTracking().SingleOrDefault(x => x.Id == id);

                if (currentQuestion == null) return false;


                ctx.Entry(currentQuestion).State = EntityState.Modified;

                ctx.SaveChanges();

                return true;
            }
        }

        public Question Get(int id)
        {
            using (var ctx = new SurveyDbContext())
            {
                return ctx.Questions.AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }

        public bool Update(Question question)
        {
            using (var ctx = new SurveyDbContext())
            {
                Option currentQuestion = ctx.Options.SingleOrDefault(x => x.Id == question.Id);

                if (currentQuestion == null) return false;

                currentQuestion.Text = question.Text;
                currentQuestion.ModifiedDate = DateTime.Now;

                ctx.Entry(currentQuestion).State = EntityState.Modified;

                ctx.SaveChanges();

                return true;
            }
        }
    }
}
